﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Collections;
using System.Threading;
using BillICTEnable;

namespace MasukUangBeneran
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        BillEnable bct = new BillEnable();
        int target = 0;
        int tabungan = 0;

        public void MasukkanDuitTesting()
        {
            
            lblTabungan.Visible = true;
            target = 7000;
            tabungan = 0;
            lblError.Text = target.ToString();
            lblTabungan.Text = tabungan.ToString();
            try
            {
                lblInfo.Text = "Masukkan uang hingga 7000";
                lblTabungan.Text = tabungan.ToString();
                while (true)
                {
                    Thread.Sleep(10);
                    //MessageBox.Show("MASUKKAN UANGNYA LALU KLIK OK ...");
                    string Amt = bct.ICT_CheckAmtEntry();
                    int AmtEntry = Convert.ToInt32(Amt);
                    //MessageBox.Show(AmtEntry.ToString());
                    Thread.Sleep(0);
                    if (AmtEntry > 0)
                    {
                        if (AmtEntry == 100000 || AmtEntry == 50000)
                        {
                            bct.ICT_RejectMoney();
                            return;
                        }

                        bct.ICT_StuckMoney();
                        Thread.Sleep(0);

                        tbUang.Text = AmtEntry.ToString();
                        
                        tabungan = tabungan + Convert.ToInt32(Amt);

                        lblError.Text = (target - tabungan).ToString();

                        lblTabungan.Text = tabungan.ToString();
                    }
                }
                
                //tabungan = tabungan + Convert.ToInt32(Amt);

                //lblInfo.Text = (target - tabungan).ToString();

                //lblTabungan.Text = tabungan.ToString();

                //Thread.Sleep(1000);
                //ICT_Disable_BV();
                //serialPortNya.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                bct.serialPort("COM6", 9600);
                bct.OpenPort1();
                bct.ICT_PowerOnBV(50);
                bct.ICT_Enable_BV();
                //Thread.Sleep(2000);
                Thread t = new Thread(new ThreadStart(MasukkanDuitTesting));
                t.Start();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                bct.ICT_Disable_BV();
                bct.ClosePort();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //lblTabungan.Visible = false;
            //target = 0;
            //tabungan = 0;
            //tbUang.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
